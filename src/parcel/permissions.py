from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """Custom permission class for verification owner of Parcel instance"""
    def has_object_permission(self, request, view, obj):
        return obj.session_key == request.session.session_key
