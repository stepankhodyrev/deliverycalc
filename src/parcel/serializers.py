from rest_framework import serializers

from .models import Parcel, ParcelType


class ParcelTypeSerializer(serializers.ModelSerializer):
    """
    Serializer for ParcelType model
    Fields:
        - name (int): Id of choice from NAME_TYPE_CHOICES list
        - get_name_display (str): String representation of selected item. Read only
    """
    class Meta:
        model = ParcelType
        fields = ('name', 'get_name_display')
        read_only_fields = ('get_name_display', )


class ParcelSerializer(serializers.ModelSerializer):
    """
    Serializer for Parcel model
    Fields:
        - id (int): Id of Parcel model. Read only
        - title (str): Title of parcel. Max 20 symbols
        - weight (int): Weight of parcel in grams
        - content_value (float): The cost of the contents of the package
        - delivery_price (Optional[float]): Cost of delivery. Optional field
        - type (ParcelTypeSerializer): Type of parcel. Link to ParcelTypeSerializer serializer
    Methods:
        - create: Used to process POST requests and creating new Parcel instance
        - update: Used to process PATCH and PUT requests and updating Parcel instance
    """
    type = ParcelTypeSerializer(many=False, required=True)

    class Meta:
        model = Parcel
        fields = ('id', 'title', 'weight', 'content_value', 'delivery_price', 'type')
        read_only_fields = ('id', )

    def create(self, validated_data):
        parcel_type, created = ParcelType.objects.get_or_create(name=validated_data.pop('type').get('name'))
        instance = Parcel.objects.create(type=parcel_type, **validated_data)
        return instance

    def update(self, instance, validated_data):
        parcel_type, created = ParcelType.objects.get_or_create(name=validated_data.pop('type').get('name'))
        instance.type = parcel_type
        instance.title = validated_data.get("title", instance.title)
        instance.weight = validated_data.get("weight", instance.weight)
        instance.content_value = validated_data.get("content_value", instance.content_value)
        instance.delivery_price = validated_data.get("delivery_price", instance.delivery_price)
        instance.save()
        return instance
