from django.db import models
from django.utils.translation import gettext_lazy as _


class Parcel(models.Model):
    """
    Model for Parcel instance. Used to store parcels history in db
    Fields:
        - session_key (str): Session key generated automatically
        - title (str): Title of parcel. Max 20 symbols
        - weight (int): Weight of parcel in grams
        – content_value (float): The cost of the contents of the package
        – delivery_price (Optional[float]): Cost of delivery. Optional field
        – type (ParcelType): Type of parcel. Link to ParcelType model
    """
    session_key = models.CharField(max_length=40, default='')
    title = models.CharField(_('Title of parcel'), max_length=20, default='')
    weight = models.IntegerField(_('Weight of parcel in grams'), default=0)
    content_value = models.FloatField(_('Value of content'), )
    delivery_price = models.FloatField(_('Cost of delivery'), blank=True, null=True)
    type = models.ForeignKey('ParcelType', verbose_name='Type of parcel',
                             related_name='parcel_type', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Parcel')
        verbose_name_plural = _('Parcels')

    def __str__(self):
        return f'{self.title}. {self.weight}. {self.content_value}. {self.type}'
