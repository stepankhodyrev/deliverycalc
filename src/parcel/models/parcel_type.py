from django.db import models
from django.utils.translation import gettext_lazy as _


class ParcelType(models.Model):
    """
    Model for Parcel type instance. Used to link type of parcels to Parcel model
    Fields:
        - name (int): Type name selected from the list of available types NAME_TYPE_CHOICES
    """
    NAME_TYPE_CHOICES = [
        (1, _("Clothes")),
        (2, _("Electronics")),
        (3, _("Other"))
    ]

    name = models.IntegerField(_("Name of type"), choices=NAME_TYPE_CHOICES, default=1)

    class Meta:
        verbose_name = _('Parcel type')
        verbose_name_plural = _('Parcel types')

    def __str__(self):
        return f'{self.get_name_display()}'
