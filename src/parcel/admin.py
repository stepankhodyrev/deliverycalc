from django.contrib import admin

from .models import Parcel, ParcelType


@admin.register(Parcel)
class ParcelAdmin(admin.ModelAdmin):
    list_display = ('title', 'weight', 'content_value', 'type')
    list_filter = ('type', 'weight')


@admin.register(ParcelType)
class ParcelTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )
