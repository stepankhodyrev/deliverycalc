import logging

import requests

from celery import shared_task

from parcel.models import Parcel


logger = logging.getLogger(__name__)


@shared_task
def check_deliveries_cost(*args, **kwargs):
    """
    Periodic task used to check all Parcel instances with empty delivery_price field and calculating it
    Run on server startup and repeat every 5 minutes (set on _project.settings CELERY_BEAT_SCHEDULE)
    Formula for calculation: Weight of Parcel (in kg) * 0.5 + Content value (in USD) * 0.01 * USD rate to Rub
    To run a function at a moment - use the <make calculation> command in terminal
    """
    logger.info('Start of checking shipping cost calculations')
    parcels = Parcel.objects.filter(delivery_price__isnull=True)
    if parcels:
        try:
            values = requests.get('https://www.cbr-xml-daily.ru/daily_json.js', timeout=5)
            values.raise_for_status()
            usd_rate = values.json().get('Valute').get('USD').get('Value')
            for i_parcel in parcels:
                delivery_cost = (((i_parcel.weight / 1000) * 0.5) + (
                        i_parcel.content_value / usd_rate * 0.01)) * usd_rate
                i_parcel.delivery_price = round(delivery_cost, 2)
                i_parcel.save()

        except requests.exceptions.RequestException as err:
            logger.error('Not implement error when getting Parcel instance')
        except requests.exceptions.HTTPError as errh:
            logger.error('Http error')
        except requests.exceptions.ConnectionError as errc:
            logger.error('Error connecting')
        except requests.exceptions.Timeout as errt:
            logger.error('Timeout error when request for usd currency')
        except AttributeError as e:
            logger.error('Error with json structure, need to check for changes')
            return 'Error with json structure for currency values'

        return f'{len(parcels)} calculated'
    return "Don't have any deliveries to calculate"
