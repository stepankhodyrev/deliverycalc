import logging

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics

from parcel.models import Parcel, ParcelType
from parcel.permissions import IsOwnerOrReadOnly
from parcel.serializers import ParcelTypeSerializer, ParcelSerializer
from utils.api.generics import DetailUpdateRemoveAPIView


logger = logging.getLogger(__name__)


class ParcelListAPIView(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """
    API View for retrieving Parcel model list
    Pagination: By default - 10 instances per page. Set in _project.settings REST_FRAMEWORK
    Methods:
        - get: Used to process GET requests and retrieving Parcel list created by the owner
        - post: Used to process POST requests and creating new Parcel instance
    """
    queryset = Parcel.objects.all()
    serializer_class = ParcelSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def get(self, request, *args, **kwargs):
        logger.info("Retrieve parcel list view", request, request.META)
        session_key = request.session.session_key
        queryset = Parcel.objects.filter(session_key=session_key).select_related('type')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            session_key = request.session.session_key
            if not session_key:
                logger.info('Creating new session key')
                request.session.create()
                session_key = request.session.session_key
            logger.info('Creating new parcel instance')
            parcel = serializer.save(session_key=session_key)
            return Response({'shipment_id': parcel.id}, status=status.HTTP_201_CREATED)
        logger.info('Validation error in serializer')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ParcelTypeListAPIView(APIView):
    """
    API View for retrieving ParcelType model list
    Methods:
        - get: Used to process GET requests and retrieving all ParcelType instances list
    """
    def get(self, request):
        logger.info("Retrieve parcel type list view", request, request.META)
        parcel_types = ParcelType.objects.all()
        serializer = ParcelTypeSerializer(parcel_types, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ParcelDetailAPIView(DetailUpdateRemoveAPIView):
    """
    API View for retrieving Parcel model detail
    Methods:
        - get: Used to process GET requests and retrieving Parcel instance
        - patch: Used to process PATCH and PUT requests and updating Parcel instance
    """
    serializer_class = ParcelSerializer
    queryset = Parcel.objects
    permission_classes = [IsOwnerOrReadOnly]

    def get(self, request, *args, **kwargs):
        logger.info("Retrieve parcel detail view", request, request.META)
        parcel_id = kwargs.get("parcel_id")
        try:
            parcel = Parcel.objects.get(id=parcel_id)
            serializer = self.serializer_class(instance=parcel)
            return Response(serializer.data)
        except Parcel.DoesNotExist:
            logger.info(f'User trying to get not found Parcel instance with {parcel_id}id')
            raise NotFound(detail=f'Not found parcel with id = {parcel_id}')
        except Exception as e:
            logger.debug('Not implement error when getting Parcel instance')
            return Response({"message": e}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, *args, **kwargs):
        logger.info("Retrieve parcel update view", request, request.META)
        parcel_id = kwargs.get("parcel_id")
        try:
            parcel = Parcel.objects.get(id=parcel_id)
            serializer = self.serializer_class(instance=parcel, data=request.data, partial=True)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            logger.info('Validation error in serializer')
            return Response(status=status.HTTP_400_BAD_REQUEST)

        except Parcel.DoesNotExist:
            logger.info(f'User trying to update not found Parcel instance with {parcel_id}id')
            raise NotFound(detail=f'Not found parcel with id = {parcel_id}')

        except Exception as e:
            logger.debug('Not implement error when updating Parcel instance')
            return Response({"message": ''}, status=status.HTTP_400_BAD_REQUEST)


class ParcelTypeDetailAPIView(DetailUpdateRemoveAPIView):
    """
    API View for retrieving ParcelType model detail
    Methods:
        - get: Used to process GET requests and retrieving ParcelType instance
    """
    lookup_field = "pk"
    lookup_url_kwarg = "parcel_type_id"
    serializer_class = ParcelTypeSerializer
    queryset = ParcelType.objects

    def get(self, request, *args, **kwargs):
        logger.info("Retrieve parcel type detail view", request, request.META)
        parcel_type_id = kwargs.get("parcel_type_id")
        try:
            parcel_type = ParcelType.objects.get(id=parcel_type_id)
            serializer = self.serializer_class(instance=parcel_type)
            return Response(serializer.data)
        except Parcel.DoesNotExist:
            logger.info(f'User trying to get not found Parcel type instance with {parcel_type_id}id')
            raise NotFound(detail=f'Not found parcel type with id = {kwargs.get("parcel_type_id")}')
        except Exception as e:
            logger.debug('Not implement error when getting Parcel type instance}')
            return Response({"message": e}, status=status.HTTP_400_BAD_REQUEST)
