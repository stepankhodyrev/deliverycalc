from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import ParcelListAPIView, ParcelDetailAPIView, ParcelTypeListAPIView, ParcelTypeDetailAPIView


urlpatterns = [
    path("parcels/", ParcelListAPIView.as_view(), name="api-parcels"),
    path("parcels/<int:parcel_id>", ParcelDetailAPIView.as_view(), name="api-parcel-detail"),
    path("parcel_types/", ParcelTypeListAPIView.as_view(), name="api-parcel-types"),
    path("parcel_types/<int:parcel_type_id>", ParcelTypeDetailAPIView.as_view(), name="api-parcel-type-detail"),
]
