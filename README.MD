# Delivery calculator app

## Contents
- [Description of the project](#description-of-project)
- [Easy start](#for-run-the-project)
- [Enpoints](#endpoints)
- [Stack](#used-stack)
- [Commands](#all-commands)

## Description of project

Microservice for International Shipping Service.
The service receives data on parcels and calculates the cost of delivery

## For run the project

- Copy env.tmplt file and paste as .env
- Install docker and docker-compose if you dont have it
- Run docker desktop on your machine
- Go to project root folder
- Use ```make build``` command in terminal
- Use ```make up``` command in terminal
 
For stop server or find another functions after start, look up on [commands section](#all-commands)

## Endpoints
- ```swagger/``` – Swagger docs path
- ```api/``` – Main path for all API endpoints
- ```api/parcels/``` – Path for get list of all parcels created by the owner or creating new instances
- ```api/parcels/<int:parcel_id>``` – Path for get detail info about certain parcell,
update fields on it or delete the instance
- ```api/parcel_types/``` – Path for get list of all parcel types
- ```api/parcel_types/<int:parcel_type_id>``` – Path for get detail info about certain parcell_type
or delete the instance


## Used stack
This is a **Django Rest Framework** project. **PostgreSQL** is used as database.
**Celery** is used to perform periodic tasks. **Redis** used as broker for Celery.

**Docker-compose** on startup creating 4 services:
- delivery-calc-db (PostgreSQL db)
- redis (Redis server for Celery)
- celery (For periodic tasks)
- delivery-calc-app (Main app)

You can see all dependencies on ```pyproject.toml``` file 

## All commands
- ```make build``` – command for building new docker container
- ```make up``` – command for run existing docker container
- ```make stop``` – command for terminate running container
- ```make restart``` – command for stop and up running container straightaway
- ```make destroy``` – command for delete existing docker container
- ```make log``` – command for displaying log from docker to terminal
- ```make makemigrations``` – command for making migrations on development
- ```make migrate``` – command for access migrations on development
- ```make calculate``` – command for run delivery price calculation func manually
